﻿// Copyright (c) 2015
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Diagnostics;

namespace DetectionTest
{
	class MainClass
	{

		public static void TS2Detect()
		{
			string gameDir = "\\The Sims 2 Ultimate Collection\\";
			string publisher = "EA GAMES";
			string regDir = "The Sims 2";
			string epDir = "\\Fun with Pets\\SP9\\TSBin";
			string isReg64bit = "";

			if ((Environment.Is64BitOperatingSystem == false) && (Environment.Is64BitProcess))
			{
				isReg64bit = "SOFTWARE"; // False
			}
			else
			{
				isReg64bit = "SOFTWARE\\Wow6432Node"; // True
			}

			RegistryKey softwareKey = Registry.LocalMachine.OpenSubKey(isReg64bit);

			if (Array.Exists(softwareKey.GetSubKeyNames(), delegate(string s)
				{
					return s.Equals(publisher, StringComparison.InvariantCultureIgnoreCase);
				}))
			{
				RegistryKey maxisKey = softwareKey.OpenSubKey(publisher);
				if (Array.Exists(maxisKey.GetSubKeyNames(), delegate(string s)
					{
						return s.Equals(regDir, StringComparison.InvariantCultureIgnoreCase);
					}))
				{
					RegistryKey ts2key = maxisKey.OpenSubKey(regDir);
					string installDir = (string)ts2key.GetValue("Install Dir");
					installDir += gameDir + epDir;
					string powersCombined = installDir;
					// MessageBox.Show(powersCombined);
					DialogResult runGame = MessageBox.Show(powersCombined, 
						                                      "Run The Sims 2?", MessageBoxButtons.YesNo);
					if (runGame == DialogResult.Yes)
					{
						Process.Start("Sims2EP9.exe", powersCombined); // opens in fullscreen!
					}
					else
					{
						Environment.Exit(0);
					}
				}
				else
				{
					MessageBox.Show("Not found!");
					Console.ReadLine();
				}
			}
			else
			{
				MessageBox.Show("Error: No Maxis products were found on your system.");
				Console.ReadLine();
			}
		}

		public static void Main(string[] args)
		{
			TS2Detect();
		}
	}
}
